/* eslint-disable no-undef */
// Remember to update `jsconfig.json`
const aliases = (prefix = `src`) => ({
    '@': `${prefix}`,
});
  
module.exports = aliases;