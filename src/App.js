import React, { useState } from 'react';

import { Provider } from 'react-redux';
import { toast } from 'react-toastify';

import api from '@/utils/api';
import configureStore from '@/store/store';
import Header from '@/components/sections/header';
import Hero from '@/components/sections/hero';
import Movies from '@/components/sections/movies';
import ErrorBoundary from '@/components/sections/movies/ErrorBoundary';
import { MovieDetailsContext } from '@/contexts/MovieDetailsContext';
import cfg from '@/cfg';

import '@/styles/app.scss';
import 'react-toastify/dist/ReactToastify.css';

const apis = {
  ajaxApi: api(),
};
const store = configureStore(apis);

toast.configure(cfg.toastConfig);

function App() {
  const [movieDetails, setMovieDetails] = useState({});

  return (
    <Provider store={store}>
      <MovieDetailsContext.Provider value={{movieDetails, setMovieDetails}}>
        <Header/>
        <Hero/>
        <ErrorBoundary>
          <Movies/>
        </ErrorBoundary>
      </MovieDetailsContext.Provider>
    </Provider>
  );
}

export default App;
