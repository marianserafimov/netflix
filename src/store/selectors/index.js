import { path } from 'ramda';

export const moviesSelector = path(['movies', 'data']);
export const filteredMoviesSelector = path(['movies', 'filteredData']);
export const moviesCountSelector = path(['movies', 'moviesCount']);

export const isPreloaderLoadingSelector = path(['preloader']);
