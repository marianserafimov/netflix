import { createAction } from 'redux-actions';
import { nthArg } from 'ramda';

import { API_REQUEST } from '../../types';

export const sendApiRequest = createAction(API_REQUEST, null, nthArg(1));
