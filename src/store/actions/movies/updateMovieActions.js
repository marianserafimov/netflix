import { UPDATE_MOVIE  } from '../../types';
import createAsyncActions from '@/utils/createAsyncActions';

export const {
  updateMovie,
  updateMoviePending,
  updateMovieFulfilled,
  updateMovieRejected,
} = createAsyncActions(UPDATE_MOVIE, 'updateMovie');
