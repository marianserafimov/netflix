import { createAction } from 'redux-actions';

import { FILTER_MOVIES_BY_GENRE } from '../../types';

export const filterMoviesByGenre = createAction(FILTER_MOVIES_BY_GENRE);
