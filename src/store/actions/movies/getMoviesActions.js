import { GET_MOVIES  } from '../../types';
import createAsyncActions from '@/utils/createAsyncActions';

export const {
  getMovies,
  getMoviesPending,
  getMoviesFulfilled,
  getMoviesRejected,
} = createAsyncActions(GET_MOVIES, 'getMovies');
