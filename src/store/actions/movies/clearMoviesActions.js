import { createAction } from 'redux-actions';

import { CLEAR_MOVIES } from '../../types';

export const clearMovies = createAction(CLEAR_MOVIES);
