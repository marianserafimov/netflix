import { ADD_MOVIE  } from '../../types';
import createAsyncActions from '@/utils/createAsyncActions';

export const {
  addMovie,
  addMoviePending,
  addMovieFulfilled,
  addMovieRejected,
} = createAsyncActions(ADD_MOVIE, 'addMovie');
