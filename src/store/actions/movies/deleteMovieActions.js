import { DELETE_MOVIE  } from '../../types';
import createAsyncActions from '@/utils/createAsyncActions';

export const {
  deleteMovie,
  deleteMoviePending,
  deleteMovieFulfilled,
  deleteMovieRejected,
} = createAsyncActions(DELETE_MOVIE, 'deleteMovie');
