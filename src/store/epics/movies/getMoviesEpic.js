import { ofType } from 'redux-observable';
import { mergeMap } from 'rxjs/operators';
import { merge, of } from 'rxjs';

import { GET_MOVIES } from '../../types';
import {
  getMoviesPending,
  getMoviesFulfilled,
  getMoviesRejected,
} from '../../actions';
import {
  openPreloaderAction,
  closePreloaderAction,
  sendApiRequest,
  notifyWarning,
  notifyError
} from '../../actions';

import generateError from '@/utils/generateError';

const createPayload = (payload) => ({
  method: 'get',
  url: 'http://localhost:4000/movies',
  onSuccess: [
    closePreloaderAction,
    (response) =>
      response.data.data.length === 0
        ? notifyWarning("No movies found")
        : getMoviesFulfilled(response),
  ],
  onError: [
    getMoviesRejected,
    closePreloaderAction,
    (error) => notifyError(generateError(error)),
  ],
  data: payload,
});

const meta = {
  api: 'ajaxApi',
};

const getMoviesEpic$ = (action$) =>
  action$.pipe(
    ofType(GET_MOVIES.DEFAULT),
    mergeMap(({ payload }) =>
      merge(
        of(getMoviesPending(), openPreloaderAction()),
        of(sendApiRequest(createPayload(payload), meta)),
      ),
    ),
  );

export default getMoviesEpic$;
