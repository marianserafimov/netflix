import { ofType } from 'redux-observable';
import { mergeMap } from 'rxjs/operators';
import { merge, of } from 'rxjs';

import { UPDATE_MOVIE } from '../../types';
import {
  updateMoviePending,
  updateMovieFulfilled,
  updateMovieRejected,
} from '../../actions';

import {
  openPreloaderAction,
  closePreloaderAction,
  sendApiRequest,
  notifyError,
  notifySuccess
} from '../../actions';

import generateError from '@/utils/generateError';

const createPayload = (payload) => ({
  method: 'put',
  url: 'http://localhost:4000/movies',
  onSuccess: [
    closePreloaderAction,
    () => notifySuccess('Movie has been updated'),
    (reponse) => updateMovieFulfilled(reponse)
  ],
  onError: [
    updateMovieRejected,
    closePreloaderAction,
    (error) => notifyError(generateError(error)),
  ],
  data: payload,
});

const meta = {
  api: 'ajaxApi',
};

const updateMovieEpic$ = (action$) =>
  action$.pipe(
    ofType(UPDATE_MOVIE.DEFAULT),
    mergeMap(({ payload }) =>
      merge(
        of(updateMoviePending(), openPreloaderAction()),
        of(sendApiRequest(createPayload(payload), meta)),
      ),
    ),
  );

export default updateMovieEpic$;
