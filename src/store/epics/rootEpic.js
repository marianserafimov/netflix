import { combineEpics } from 'redux-observable';

import apiRequestEpic$ from './apiRequestEpic';

import getMoviesEpic$ from './movies/getMoviesEpic';
import deleteMovieEpic$ from './movies/deleteMovieEpic';
import updateMovieEpic$ from './movies/updateMovieEpic';
import addMovieEpic$ from './movies/addMovieEpic';

const rootEpic = (apis) =>
  combineEpics(
    apiRequestEpic$(apis),
    getMoviesEpic$,
    deleteMovieEpic$,
    updateMovieEpic$,
    addMovieEpic$,
  );

export default rootEpic;
