import { createReducer } from 'redux-create-reducer';
import { toast } from 'react-toastify';

import { NOTIFY_SUCCESS, NOTIFY_ERROR, NOTIFY_WARNING } from '../types';

const initialState = { type: '', message: '' };

const notificationsReducer = createReducer(initialState, {
  [NOTIFY_SUCCESS]: (state, { payload }) => {
    toast.success(payload, {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: false,
    });
    return { type: 'SUCCESS', message: payload };
  },
  [NOTIFY_ERROR]: (state, { payload }) => {
    toast.error(payload, {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: false,
    });
    return { type: 'ERROR', message: payload };
  },
  [NOTIFY_WARNING]: (state, { payload }) => {
    toast.warn(payload, {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: false,
    });
    return { type: 'WARN', message: payload };
  },
});

export default notificationsReducer;