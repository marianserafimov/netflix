import axios from 'axios';

const api = () => ({
  exec: ({ method, data, url }) => {
    const reqConfig = {
      method,
      url,
      headers: {
        // "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, HEAD, OPTIONS",
        "Access-Control-Allow-Credentials": true
      },
    };

    if (method.toLowerCase() === 'get') {
      reqConfig.params = data;
    } else {
      reqConfig.data = data;
    }

    return axios(reqConfig);
  },
});

export default api;
