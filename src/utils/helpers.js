export const modifyGenres = (genres) => {
    return genres && genres.length
        ? genres.length == 2
            ? `${genres[0]} & ${genres[1]}`
            : genres.join(',')
        : 'No genres'
}

export const minutesToHours = (time) => {
    let minutes = time % 60;
    let hours = (time - minutes) / 60;

    return `${hours}h ${minutes}min`;
}

export const sortMoviesByDate = (movies, sortType) => {
    return movies.sort((a, b) => {
        let dateA = new Date(a.release_date).getTime();
        let dateB = new Date(b.release_date).getTime();
        let caseOperator = '';

        switch (sortType) {
            case 'newest':
                caseOperator = dateA < dateB;
              break;
            case 'oldest':
                caseOperator = dateA > dateB;
              break;
            default:
                caseOperator = dateA < dateB;
        }

        return caseOperator ? 1 : -1; 
    });
}