import React from 'react';

import * as PropTypes from 'prop-types';
import Btn from '../../btn';
import CloseBtn from '../../close-btn';

import "./style.scss"
function ConfirmationModal(props) {
  const onClose = () => {
    props.onClose();
  }

  const onConfirm = () => {
    props.onConfirm();
    props.onClose();
  }
  const { className, description, heading } = props;
  return (
    <div className={`modal-main confirmation-modal ${className || ''}`} onClick={onClose}>
      <div className="main-modal-container" onClick={(event) => event.stopPropagation()}>
        <CloseBtn onClick={onClose}/>
        <h1>{heading}</h1>
        <p className="description">{description}</p>
        <div className="form-buttons">
          <Btn label="submit" className="btn-primary" onClick={onConfirm}/>
        </div>
      </div>
    </div>
  );
}

ConfirmationModal.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  heading: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default ConfirmationModal;
