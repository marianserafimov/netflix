import React from 'react';

import * as PropTypes from 'prop-types';

import Btn from '../../btn';
import CloseBtn from '../../close-btn';

import "./style.scss"
function FormModal(props) {
  const onClose = () => {
    props.onClose();
  }
  const { className, children, formId, heading } = props;
  return (
    <div className={`modal-main form-modal ${className || ''}`} onClick={onClose}>
      <div
        className="main-modal-container"
        onClick={(event) => event.stopPropagation()}
      >
        <CloseBtn onClick={onClose}/>
        <h1>{heading}</h1>
        {children}
        <div className="form-buttons">
          <Btn onClick={onClose} label="reset" className="btn-transparent"/>
          <Btn label="submit" className="btn-primary" type={'submit'} formId={formId}/>
        </div>
      </div>
    </div>
  );
}

FormModal.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
  formId: PropTypes.string,
  heading: PropTypes.string,
};

export default FormModal;
