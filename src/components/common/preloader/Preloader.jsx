import React from 'react';

import * as PropTypes from 'prop-types';
import { Spinner } from 'reactstrap';

import "./style.scss"

function Preloader({ className }) {
  return (
    <div className={`preloader black-overlay${className || ''}`}>
      <Spinner animation="grow" />
    </div>
  );
}

Preloader.propTypes = {
  className: PropTypes.string,
};

export default Preloader;
