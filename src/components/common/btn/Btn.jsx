import React from 'react';

import * as PropTypes from 'prop-types';

import './style.scss';

function Btn({label, onClick, className, type="button", formId=""}) {
  return (
    <button
      type={type}
      className={`btn-main ${className ? className : ''}`}
      onClick={onClick}
      form={formId}
    >
      {label}
    </button>
  );
}

Btn.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string.isRequired,
  type: PropTypes.string,
  formId: PropTypes.string
};

export default Btn;
