import React from 'react';

import * as PropTypes from 'prop-types';

import './style.scss'; 

function ValidationError({ error }) {
  
  return (
    <div className="formik-validation-error">
      {error
        ? error
        : null}
    </div>
  );
}

ValidationError.propTypes = {
  error: PropTypes.string.isRequired,
};

export default ValidationError;
