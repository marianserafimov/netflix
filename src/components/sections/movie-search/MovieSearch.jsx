import React from 'react';

import { Container, Input } from 'reactstrap';
import Btn from '../../common/btn';
import cfg from '@/cfg';

import hero from '@/assets/hero.png';
import './style.scss';

function MovieSearch() {
  const { texts } = cfg;

  return (
    <div className="movie-search">
      <img src={hero} alt={texts.movieSearchImgAlt}/>
      <Container>
        <h1>{texts.movieSearchHeading}</h1>
        <div className="d-flex mt-5">
            <Input placeholder={texts.movieSearchInputPLaceholder}/>
            <Btn label={texts.movieSearchBtnLabel} className="btn-primary btn-big"/>
        </div>
      </Container>
    </div>
  );
}

export default MovieSearch;
