import React, {useContext} from 'react';

import { MovieDetailsContext } from '@/contexts/MovieDetailsContext'
import MovieDetails from '../movie-details';
import MovieSearch from '../movie-search';

import './style.scss';

function Hero() {
  const { movieDetails } = useContext(MovieDetailsContext);

  return (
    <div className="hero">
      {Object.keys(movieDetails).length
        ? <MovieDetails movie={movieDetails} />
        : <MovieSearch />
      }
    </div>
  );
}

export default Hero;
