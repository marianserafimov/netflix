import React, {useState} from 'react';

import { useDispatch } from 'react-redux';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { sortMoviesByDate } from '@/store/actions';
import cfg from '@/cfg';

import './style.scss'

function Sorting() {
  const dispatch = useDispatch();
  const { sortingFilters, texts } = cfg;

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [activeSort, setActiveSort] = useState(sortingFilters[0].value);

  const toggleDropdown = () => setDropdownOpen(prevState => !prevState);

  function filterByDate(value) {
    setActiveSort(value);
    dispatch(sortMoviesByDate(value));
  }

  const activeSortIndex = sortingFilters.findIndex(sort => sort.value === activeSort);

  return (
    <div className="sorting">
      <h4>{texts.sortBy}</h4>

      <Dropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
        <DropdownToggle caret>
          {sortingFilters[activeSortIndex].label}
        </DropdownToggle>
        <DropdownMenu>
          {sortingFilters.map(({ label, value }, index) =>
            <DropdownItem onClick={() => filterByDate(value)} className={value === activeSort ? 'active' : ''} key={`${value}-${index}`}>
              {label}
            </DropdownItem>
          )}
        </DropdownMenu>
      </Dropdown>
    </div>
  );
}

export default Sorting;
