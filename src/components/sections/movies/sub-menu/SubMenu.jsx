import React from 'react';

import Filters from './filters';
import Sorting from './sorting';

import { useSelector } from 'react-redux';
import { moviesCountSelector } from '@/store/selectors';

import './style.scss'

function SubMenu() {
  const moviesCount = useSelector(moviesCountSelector);

  return (
    <div className="sub-menu">
      <div className="menu-sections">
        <Filters/>
        <Sorting/>
      </div>
      <div className="sub-menu-line">
        <div/>
        <div/>
      </div>
      <div className="movies-count">
        {moviesCount}
        <span>movies found</span>
      </div>
    </div>
  );
}

export default SubMenu;
