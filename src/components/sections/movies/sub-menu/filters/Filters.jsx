import React, { useState } from 'react';

import { useDispatch } from 'react-redux';

import { filterMoviesByGenre } from '@/store/actions';
import cfg from '@/cfg';

import './style.scss'

function Filters() {
  const dispatch = useDispatch();
  const { genresFilters } = cfg;

  const [activeGenre, setActiveGenre] = useState(genresFilters[0].value);

  function filterByGenre(choosenGenre) {
    if(choosenGenre !== activeGenre) {
      setActiveGenre(choosenGenre);
      dispatch(filterMoviesByGenre(choosenGenre));
    }
  }

  return (
    <ul className="filters">
      {genresFilters.map(({ label, value }, index) => 
        <li
          key={`${value}-${index}`}
          className={activeGenre === value ? 'active' : ''}
          onClick={() => filterByGenre(value)}
        >
          {label}
        </li>
      )}
    </ul>
  );
}

export default Filters;
