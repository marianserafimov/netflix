import React, {useState, useContext} from 'react';

import * as PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import CloseBtn from '../../../common/close-btn';
import FormModal from '../../../common/modals/form-modal';
import ConfirmationModal from '../../../common/modals/confirmation-modal';
import MovieForm from '../../../common/forms/movie-form';
import { MovieDetailsContext } from '@/contexts/MovieDetailsContext';
import { modifyGenres } from '@/utils/helpers';
import { deleteMovie } from '@/store/actions';
import cfg from '@/cfg';

import './style.scss';
function MovieItem({ movie }) {
  const dispatch = useDispatch();

  const [isInfoBtnActive, setIsInfoBtnActive] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);

  const {setMovieDetails} = useContext(MovieDetailsContext);

  const { modalTypes, texts, formIds } = cfg;
  
  const {
    title,
    poster_path,
    release_date,
    genres,
    id
  } = movie;

  const triggerInfoBtn = (e) => {
    e.stopPropagation();
    
    setIsInfoBtnActive(!isInfoBtnActive);
  }

  const toggleModal = (event, modal) => {
    event.stopPropagation();

    modal === modalTypes.edit && setEditModal(!editModal);
    modal === modalTypes.delete && setDeleteModal(!deleteModal);
  }

  const deleteMovieSubmit = () => {
    dispatch(deleteMovie({id}))
  }

  return (
      <div className="movie-item" onClick={() => setMovieDetails(movie)}>
        <div className="info-btn" onClick={triggerInfoBtn}>
          <div/>
          <div/>
          <div/>
        </div>
        {isInfoBtnActive && 
            <ul>
              <CloseBtn onClick={triggerInfoBtn}/>
              <li onClick={() => toggleModal(event, modalTypes.edit)}>{texts.edit}</li>
              <li onClick={() => toggleModal(event, modalTypes.delete)}>{texts.delete}</li>
            </ul>
          }
        <img src={poster_path} alt={title}/>
        <div className="movie-heading">
          <h3>{title}</h3>
          <span>{release_date}</span>
        </div>
        <p>
          {modifyGenres(genres)}
        </p>
        {editModal && (
          <FormModal
            onClose={() => toggleModal(event, modalTypes.edit)}
            formId={formIds.editMovieFormId}
            heading={texts.editModal.heading}
          >
            <MovieForm
              movieInitialValues={movie}
              formId={formIds.editMovieFormId}
              afterSubmit={() => toggleModal(event, modalTypes.edit)}
            />
          </FormModal>
        )}

        {deleteModal && (
          <ConfirmationModal
            description={texts.confirmationModal.description}
            heading={texts.confirmationModal.heading}
            onClose={() => toggleModal(event, modalTypes.delete)}
            onConfirm={deleteMovieSubmit}
          />
        )}
      </div>
  );
}

MovieItem.propTypes = {
  movie: PropTypes.object.isRequired,
};

export default MovieItem;
