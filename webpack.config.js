/* eslint-disable no-undef */
const path = require('path');

const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

const alias = require(`./src/cfg/aliases`);

const SRC = "./src";
const aliases = alias(SRC);

const resolvedAliases = Object.fromEntries(
  Object.entries(aliases).map(([key, value]) => [
    key,
    path.resolve(__dirname, value),
  ])
);

module.exports = {
  plugins: [
      new CaseSensitivePathsPlugin(),
      new HtmlWebpackPlugin({
          template: './public/index.html',
          filename: './index.html',
          favicon: './public/favicon.ico'
      })
  ],
  mode: 'development',
  entry: path.resolve(__dirname, './src'),
  module: {
    rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader'],
        },
        {
            test: /\.s?css$/,
            use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
            test: /\.(png|svg|jpg|jpeg|gif)$/i,
            type: 'asset',
        },  
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: resolvedAliases
  },
  output: {
    path: path.resolve(__dirname, './public'),
    filename: 'bundle.js',
  },
};